import { Component, OnInit } from '@angular/core';

import {TranslateService} from '@ngx-translate/core';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.sass']
})
export class NavComponent implements OnInit {

  cssUrl: string;
  bootstrapUrl: string;
  fontAwesome: string;

  constructor(private translate: TranslateService, public sanitizer: DomSanitizer) {
    translate.setDefaultLang('en');
  }

  ngOnInit() {
    let currentLang = "en"
    if (this.translate.currentLang != null){
      currentLang = this.translate.currentLang;
    }
    this.cssUrl = currentLang === "en" ? '/assets/stylesMain.sass' : '/assets/stylesRTL.css';
    this.bootstrapUrl = currentLang === "en" ? '/assets/bootstrap.min.css' : '/assets/bootstrap-rtl.min.css';
  }

  useLanguage(language: string) {
    this.translate.use(language);
    this.cssUrl = language === "en" ? '/assets/stylesMain.sass' : '/assets/stylesRTL.css';
    this.bootstrapUrl = language === "en" ? '/assets/bootstrap.min.css' : '/assets/bootstrap-rtl.min.css';
  }

}
